﻿using MonoGame.Forms.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace TileMapEditor
{
    //Classe supportant l'affichage du tileset sur la droite
    class TileDisplay : GraphicsDeviceControl
    {
        Editor editor;
        Image image;
        SpriteBatch spriteBatch;
        // Ici, on utilise 4 images pour le selecteur pour deux raisons :
        // Pas besoin de redimensioner si on change la dimension des tiles
        // On souhaite autoriser la sélection multiple de tiles (autrement il faudrait redimensioner sans arrêt, plus sale)
        List <Image> selector;
        bool isMouseDown;
        Vector2 mousePosition, clickPosition;
        
        
        public TileDisplay()
        {

        }

        // On passe l'éditeur à cette fenêtre pour récupérer le sélecteur
        public TileDisplay(Editor editor)
        {
            this.editor = editor;
            editor.OnInitialize += LoadContent;
            isMouseDown = false;
        }

        private void LoadContent(object sender, EventArgs e)
        {
            image = editor.CurrentLayer.Image;
            selector = editor.Selector;
        }

        protected override void Initialize()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            MouseDown += TileDisplay_MouseDown;
            MouseUp += delegate 
            {
                isMouseDown = false;

                // On passe le selecteur de l'éditeur et on lui effecte ce qui est sélectionné
                List<Image> selector = editor.Selector;
                editor.SelectedTileRegion = new Rectangle((int)selector[0].Position.X, (int)selector[0].Position.Y, 
                    (int)(selector[1].Position.X - selector[0].Position.X), (int)(selector[2].Position.Y - selector[0].Position.Y));

                // A modifier si on modifie la taille des tiles (à rendre dynamique)
                editor.SelectedTileRegion.X /= 32;
                editor.SelectedTileRegion.Y /= 32;
                editor.SelectedTileRegion.Width /= 32;
                editor.SelectedTileRegion.Height /= 32;
            };
            MouseMove += TileDisplay_MouseMove;
        }

        //Méthode appelée lorsque l'on click sur la souris
        void TileDisplay_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (!isMouseDown)
            {
                clickPosition = mousePosition;
                foreach(Image img in selector)
                    img.Position = mousePosition;
            }

            isMouseDown = true;
            Invalidate();
        }

        //Méthode appelée lors de mouvements de la souris
        void TileDisplay_MouseMove(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            //On récupère le "numéro de tile" sur lequel se trouve le curseur (on cast en int donc on arrondi automatiquement à l'inférieur)
            mousePosition = new Vector2((int)(e.X / editor.CurrentLayer.TileDimensions.X), (int)(e.Y / editor.CurrentLayer.TileDimensions.Y));
            //On multiplie par la largeur d'une tile pour avoir la position en pixels
            // Penser à rendre cette valeur dynamique si on souhaite utiliser des tailles de tiles différentes
            mousePosition *= 32;

            if (mousePosition != clickPosition && isMouseDown)
            {
                // Positionnement des différentes parties du sélecteur en cas de sélection "multiple"
                // Drag du curseur
                for(int i = 0; i < 4; i++)
                {
                    //On souhaite exécuter les deux blocs séparément pour gérer les deux coordonnées séparément
                    if (i % 2 == 0 && mousePosition.X < clickPosition.X)
                        selector[i].Position.X = mousePosition.X;
                    else if (i % 2 != 0 && mousePosition.X > clickPosition.X)
                        selector[i].Position.X = mousePosition.X;
                    if (i < 2 && mousePosition.Y < clickPosition.Y)
                        selector[i].Position.Y = mousePosition.Y;
                    else if (i >= 2 && mousePosition.Y > clickPosition.Y)
                        selector[i].Position.Y = mousePosition.Y;
                }
                // On fait appel à invalidate ici afin que le curseur ne suive pas la souris 
                // On ne souhaite ce comportement que dans l'éditeur et non pas le displayeur de tiles
                Invalidate();
            }
            else if (isMouseDown)
            {
                foreach(Image img in selector)
                    img.Position = mousePosition;
            }

            Invalidate();
        }

        protected override void Draw()
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);
            
            image.Draw(spriteBatch);
            //On dessine le curseur en second afin qu'il ne soit pas caché derrière les tiles
            foreach (Image img in selector)
                img.Draw(spriteBatch);
        }
    }
}
