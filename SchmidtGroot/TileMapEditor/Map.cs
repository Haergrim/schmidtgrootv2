﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.IO;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace TileMapEditor
{
    //Classe représentant la map à dessiner dans l'éditeur
    public class Map
    {
        [XmlElement("Layer")]
        public List<Layer> Layer;
        public Vector2 TileDimensions;

        public void Initialize(ContentManager content)
        {
            foreach (Layer l in Layer)
                l.Initialize(content, TileDimensions);
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            foreach (Layer l in Layer)
                l.Draw(spriteBatch);
        }

        public void Save(string filePath)
        {
            // Pour chaque layer de la map, on sauvegarde
            foreach(Layer l in Layer)            
                l.Save();

            //On serialize l'objet au format XML avant de le sauvegarder
            XmlSerializer xml = new XmlSerializer(this.GetType());
            using (StreamWriter writer = new StreamWriter(filePath))
                xml.Serialize(writer, this);
        }
    }
}
