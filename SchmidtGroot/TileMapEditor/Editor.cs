﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.IO;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace TileMapEditor
{
    //Classe de la fenêtre d'édition de la tileMap
    //Héritage de la librairie Forms qui permet de plugguer monogame sur windowsform
    class Editor : MonoGame.Forms.Controls.InvalidationControl
    {

        ContentManager content;
        SpriteBatch spriteBatch;
        Map map;
        int layerNumber;
        bool isMouseDown = false;
        public List<Image> Selector;
        public Rectangle SelectedTileRegion;
        public List<Vector2> SelectedTiles;
        public Vector2 SelectorDimensions;
        Vector2 mousePosition;
        bool mouseOnScreen = false;
        string[] selectorPath = { "Editor/SelectorT1", "Editor/SelectorT2", "Editor/SelectorB1", "Editor/SelectorB2" };

        public event EventHandler OnInitialize;

        public Map Map
        {
            get { return map; }
            set { map = value;  }
        }

        public ContentManager Content
        {
            get { return content; }
        }

        public Editor()
        {
            map = new Map();
            layerNumber = 0;
            Selector = new List<Image>();

            for (int i = 0; i < 4; i++)
                Selector.Add(new Image());

            SelectorDimensions = Vector2.Zero;
            SelectedTileRegion = new Rectangle(0, 0, 0, 0);

            // Par défaut on sélectionne la première tile pour éviter des soucis
            SelectedTiles = new List<Vector2>();
            SelectedTiles.Add(Vector2.Zero);
            MouseMove += Editor_MouseMove;
            MouseDown += Editor_MouseDown;
            MouseUp += delegate { isMouseDown = false; };
            // Ici on manage l'entrée et la sortie de la souris de l'écran
            // Lorsque la souris sort, on redessine le tout à l'état de base
            MouseEnter += delegate { mouseOnScreen = true; };
            MouseLeave += delegate { mouseOnScreen = false; Draw(); Invalidate(); };
        }

        //Lorsque l'on click avec la souris, on fait appel à cette méthode
        private void Editor_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            //Remplace les tiles sur le layer actuellement sélectionné
            CurrentLayer.ReplaceTiles(mousePosition, SelectedTileRegion);
            isMouseDown = true;
        }

        private void Editor_MouseMove(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            // Lorsque l'on déplace la souris, on affiche le sélecteur
            mousePosition = new Vector2((int)(e.X / CurrentLayer.TileDimensions.X), (int)(e.Y / CurrentLayer.TileDimensions.Y));
            mousePosition *= 32;

            //On définit la hauteur et largeur de la zone sélectionnée
            int width = (int)(SelectedTileRegion.Width * CurrentLayer.TileDimensions.X);
            int height = (int)(SelectedTileRegion.Height * CurrentLayer.TileDimensions.Y);

            //Positionnement des 4 parties du sélecteur (au cas où sélection multiple de tiles)
            Selector[0].Position = mousePosition;
            Selector[1].Position = new Vector2(mousePosition.X + width, mousePosition.Y);
            Selector[2].Position = new Vector2(mousePosition.X, mousePosition.Y + height);
            Selector[3].Position = new Vector2(mousePosition.X + width, mousePosition.Y + height);

            if (isMouseDown)
                Editor_MouseDown(this, null);

            Invalidate();
        }

        //Layer actuellement sélectionné, celui sur lequel on fait la modification
        public Layer CurrentLayer
        {
            get { return map.Layer[layerNumber]; }
        }

        //Correspond à l'index du layer dans la liste de layer présent dans la map, on s'en sert pour mettre à jour le currentLayer
        public int LayerNumber
        {
            get { return layerNumber; }
            set { layerNumber = value; }
        }

        protected override void Initialize()
        {
            base.Initialize();
            content = new ContentManager(Services, "Content");
            spriteBatch = new SpriteBatch(GraphicsDevice);

            //On charge les 4 "parties" du selecteur
            for (int i = 0; i < 4; i++)
            {
                Selector[i].Path = selectorPath[i];
                Selector[i].Initialize(content);
            }

            //Chargement de la Map1 par défaut (attention donc à bien l'avoir dans le dossier Load sinon crash)
            //A modifier plus tard
            XmlSerializer xml = new XmlSerializer(map.GetType());
            Stream stream = File.Open("Load/Map1.xml", FileMode.Open);
            map = (Map)xml.Deserialize(stream);
            map.Initialize(content);

            if (OnInitialize != null)
                OnInitialize(this, null);
        }

        protected override void Draw()
        {
            //Optionnel, dessine le fond de base de monogame (bleu cornflower)
            base.Draw();
            
            //On dessigne la map
            map.Draw(spriteBatch);

            //Si la souris est sur l'éditeur, on dessine les 4 parties du sélecteur
            //Pas besoin de le dessiner autrement
            if(mouseOnScreen)
            {
                foreach (Image img in Selector)
                    img.Draw(spriteBatch);
            }

        }
    }
}
