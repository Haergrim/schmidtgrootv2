﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;
using System.IO;

namespace TileMapEditor
{
    // Classe représentant l'application complète
    public partial class Form1 : Form
    {
        public Form1()
        {
            //Initialisation des composants
            InitializeComponent();

            // Une fois cela fait, on sélectionne par défaut le radiobutton1 (premier layer)
            radioButton1.Checked = true;
        }
        
        //Méthode appelée au click sur l'item de menu "save map"
        private void saveMapToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "Xml File (.xml)|*.xml";
            sfd.Title = "Save Map";

            if (sfd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                editor1.Map.Save(sfd.FileName);
        }

        //Méthode appelée au click sur l'item de menu "load map"
        private void loadMapToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "Xml File (.xml)|*.xml";
            ofd.Title = "Load Map";

            if(ofd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                XmlSerializer xml = new XmlSerializer(editor1.Map.GetType());
                using(StreamReader reader = new StreamReader(ofd.FileName))
                {
                    editor1.Map = (Map)xml.Deserialize(reader);
                    editor1.Map.Initialize(editor1.Content);
                }
            }
        }

        //Deux radio buttons pour les deux layers, valeur des layers dans la liste écrit en dur, à modifier si on veut en ajouter plus
        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            //Modifie le layerNumber sélectionné, ce qui va impacter le currentLayer présent dans la classe editor
            editor1.LayerNumber = 0;
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            editor1.LayerNumber = 1;
        }
    }
}
