﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace SchmidtGroot
{
    //Classe d'écran de combat
    public class FightScreen : GameScreen
    {
        Player player;
        Monster monster;
        public Image Image;
        //MenuManager menuManager;

        public FightScreen()
        {
            //menuManager = new MenuManager();
        }

        public override void LoadContent()
        {
            base.LoadContent();

            //On charge le fond, personnage et le monstre
            Image.LoadContent();
            XmlManager<Player> playerLoader = new XmlManager<Player>();
            XmlManager<Monster> monsterLoader = new XmlManager<Monster>();
            player = playerLoader.Load("Load/Gameplay/Player.xml");
            monster = monsterLoader.Load("Load/Gameplay/Monster.xml");
            player.LoadContent();
            monster.LoadContent();
            //menuManager.LoadContent("Load/Menus/FightMenu.Xml");

            //Override de quelques valeurs spécifiques
            player.IsFighting = true;
            player.Image.Position.X = 550;
            player.Image.Position.Y = 220;
        }

        public override void UnloadContent()
        {
            base.UnloadContent();
            Image.UnloadContent();
            player.UnloadContent();
            monster.UnloadContent();
            //menuManager.UnloadContent();
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            player.Update(gameTime);
            monster.Update(gameTime);
            Image.Update(gameTime);
            //menuManager.Update(gameTime);

            
            //TODO : pour tests, à retirer ensuite
            if (InputManager.Instance.KeyPressed(Keys.Enter))
            {
                player.IsFighting = false;
                ScreenManager.Instance.ChangeScreens("GameplayScreen", true);
            }            
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            base.Draw(spriteBatch);
            Image.Draw(spriteBatch);
            monster.Draw(spriteBatch);
            player.Draw(spriteBatch);
            //menuManager.Draw(spriteBatch);
        }
    }
}
