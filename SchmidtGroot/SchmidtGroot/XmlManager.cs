﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.IO;

namespace SchmidtGroot
{
    //Classse XML manager permettant de serializer/deserializer des fichiers xml dans des objets c#
    public class XmlManager<T>
    {
        public Type Type;

        // Constructeur permettant de créer un objet issu de la classe passée à la construction
        public XmlManager()
        {
            Type = typeof(T);
        }

        // Méthode permettant de charger dans l'instance d'objet que l'on a créé, les propriétés qui correspondent à des valeurs présentes dans le xml du même nom
        public T Load(string path)
        {
            T instance;
            using (TextReader reader = new StreamReader(path))
            {
                XmlSerializer xml = new XmlSerializer(Type);
                instance = (T)xml.Deserialize(reader);
            }
            return instance;
        }

        public void Save(string path, object obj)
        {
            using(TextWriter writer = new StreamWriter(path))
            {
                XmlSerializer xml = new XmlSerializer(Type);
                xml.Serialize(writer, obj);
            }
        }
    }
}
