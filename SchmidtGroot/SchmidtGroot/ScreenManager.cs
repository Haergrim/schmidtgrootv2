﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.IO;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace SchmidtGroot
{
    //Singleton assurant la gestion des différents écrans du jeu
    public class ScreenManager
    {
        //La classe contient une instance de cette classe afin de s'assurer qu'on utilise toujours le même objet
        private static ScreenManager instance;
        [XmlIgnore]
        public Vector2 Dimensions { private set; get; }
        [XmlIgnore]
        public ContentManager Content { private set; get; }
        XmlManager<GameScreen> xmlGameScreenManager;

        GameScreen lastScreen, currentScreen, newScreen;
        [XmlIgnore]
        public GraphicsDevice GraphicsDevice;
        [XmlIgnore]
        public SpriteBatch SpriteBatch;

        public Image Image;
        [XmlIgnore]
        public bool IsTransitioning { get; private set; }

        //Si l'instance n'est pas déjà existante, on la créée
        public static ScreenManager Instance
        {
            get
            {
                if (instance == null)
                { 
                    //On créée une instance de le classe à l'aide du paramétrage défini dans le fichier XML correspondant
                    XmlManager<ScreenManager> xml = new XmlManager<ScreenManager>();
                    instance = xml.Load("Load/ScreenManager.xml");
                }

                //Dans tous les cas, on retourne l'instance, soit l'existante, soit celle que l'on vient de créer
                return instance;
            }
        }
        
        // Méthode permettant de préparer le nouvel écran sur lequel on va basculer
        public void ChangeScreens (string screenName, bool reuse = false)
        {
            //Permet de réutiliser l'écran précédent sans recréer un nouvel objet
            if(reuse)
            {
                newScreen = lastScreen;
            }
            else
            {
                // Création d'une nouvelle instance en fonction du nom de l'écran souhaité + set des variables associées
                newScreen = (GameScreen)Activator.CreateInstance(Type.GetType("SchmidtGroot." + screenName));
            }

            Image.IsActive = true;
            Image.FadeEffect.Increase = true;
            Image.Alpha = 0.0f;
            IsTransitioning = true;            
        }

        // Méthode assurant les transitions de l'écran courant au nouvel écran
        void Transition(GameTime gameTime)
        {
            // Si l'on est en train d'effectuer une transition, plusieurs cas possibles :
            if(IsTransitioning)
            {
                Image.Update(gameTime);

                // L'image de transition est entièrement affichée (l'écran est noir) => On charge le nouvel écran et unload le précédent
                if (Image.Alpha == 1.0f)
                {
                    lastScreen = currentScreen;
                    currentScreen.UnloadContent();
                    currentScreen = newScreen;
                    xmlGameScreenManager.Type = currentScreen.Type;
                    if (File.Exists(currentScreen.XmlPath))
                        currentScreen = xmlGameScreenManager.Load(currentScreen.XmlPath);
                    currentScreen.LoadContent();
                }
                // L'image de transition a disparue (on voit le nouvel écran) => On désactive l'image de transition et on notifie la fin de la transition
                else if (Image.Alpha == 0.0f)
                {
                    Image.IsActive = false;
                    IsTransitioning = false;
                }
            }
        }

        // Constructeur du screenManager appelé au lancement du jeu
        // Le constructeur est appelé via le xmlManager, lorsque l'on appelle l'instance du screenmanager pour la première fois
        public ScreenManager()
        {
            // Défini la taille de l'écran 
            Dimensions = new Vector2(640, 480);

            // On défini la splashscreen comme étant l'écran de base au lancement, on charge ses propriétés via le XmlManager
            currentScreen = new SplashScreen();
            //currentScreen = new GameplayScreen();
            xmlGameScreenManager = new XmlManager<GameScreen>();
            xmlGameScreenManager.Type = currentScreen.Type;
            currentScreen = xmlGameScreenManager.Load("Load/SplashScreen.xml");
        }

        public void LoadContent(ContentManager Content)
        {
            this.Content = new ContentManager(Content.ServiceProvider, "Content");
            currentScreen.LoadContent();
            Image.LoadContent();
        }

        public void UnloadContent()
        {
            currentScreen.UnloadContent();
            Image.UnloadContent();
        }

        public void Update(GameTime gameTime)
        {
            currentScreen.Update(gameTime);
            // Appel à transition, qui effectuera la transition si elle a été demandée (isTransitioning = true)
            Transition(gameTime);
        }

        public void Draw (SpriteBatch spriteBatch)
        {
            currentScreen.Draw(spriteBatch);
            // Si on est en train de transitionner, on dessine l'image de transition
            if (IsTransitioning)
                Image.Draw(SpriteBatch);
        }
    }
}
