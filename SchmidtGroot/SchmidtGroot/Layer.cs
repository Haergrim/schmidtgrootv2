﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace SchmidtGroot
{
    public class Layer
    {
        public class TileMap
        {
            [XmlElement("Row")]
            public List<string> Row;

            public TileMap()
            {
                Row = new List<string>();
            }
        }

        [XmlElement("TileMap")]
        public TileMap Tile;
        public Image Image;
        public string SolidTiles, OverlayTiles;
        List<Tile> underlayTiles, overlayTiles;
        string state;

        public Layer()
        {
            Image = new Image();

            //Chaque layer comporte des tiles à dessiner par dessus et par dessous le personnage
            underlayTiles = new List<Tile>();
            overlayTiles = new List<Tile>();
            SolidTiles = String.Empty;
            OverlayTiles = String.Empty;
        }

        public void LoadContent(Vector2 tileDimensions)
        {
            Image.LoadContent();
            Vector2 position = -tileDimensions;

            //Pour chaque row (ligne) du layer
            foreach(string row in Tile.Row)
            {
                //On split la chaine selon le caractère de fin
                string[] split = row.Split(']');
                position.X = -tileDimensions.X;
                position.Y += tileDimensions.Y;

                //Pour chaque élément "[0:0" par exemple, on effectue des opérations de traitement
                foreach(string s in split)
                {
                    if(s != String.Empty)
                    {
                        position.X += tileDimensions.X;
                        //Ne pas créer de tile pour ce caractère dans la map
                        if(!s.Contains("x"))
                        {
                            state = "Passive";
                            Tile tile = new Tile();

                            //Suppression du bracket ouvrant (remplacé par vide)
                            string str = s.Replace("[", String.Empty);
                            //Récupération valeurs 1 et 2 (de chaque côté du :)
                            int value1 = int.Parse(str.Substring(0, str.IndexOf(':')));
                            int value2 = int.Parse(str.Substring(str.IndexOf(':') +1 ));

                            //Si la tile ainsi récupérée est présente dans la liste des SolideTiles, on lui affecte le statut Solid
                            //Attention, cela signifie donc qu'une tile déclarée comme solid le sera pour toutes ses itérations dans ce layer
                            if (SolidTiles.Contains("[" + value1.ToString() + ":" + value2.ToString() + "]"))
                                state = "Solid";
                            
                            //Load la tile
                            tile.LoadContent(position, new Rectangle(
                                value1 * (int)tileDimensions.X, value2 * (int)tileDimensions.Y,
                                (int)tileDimensions.X, (int)tileDimensions.Y), state);
                            //Si la tile est dans la liste des overlay, on ajoute à la liste correspondante
                            //Sinon, on ajoute à la liste des underlay (par défaut)
                            //Comme pour les solidTiles, il suffit d'ajouter chaque tile de façon unique à cette liste du layer pour qu'elle soit toujours dans cette configuration
                            if(OverlayTiles.Contains("[" + value1.ToString() + ":" + value2.ToString() + "]"))
                                overlayTiles.Add(tile);
                            else
                                underlayTiles.Add(tile);
                        }
                    }
                }
            }
        }

        public void UnloadContent()
        {
            Image.UnloadContent();
        }

        public void Update(GameTime gameTime, ref Player player)
        {
            foreach(Tile tile in underlayTiles)
                tile.Update(gameTime, ref player);
            foreach (Tile tile in overlayTiles)
                tile.Update(gameTime, ref player);
        }

        public void Draw(SpriteBatch spriteBatch, string drawType)
        {
            List<Tile> tiles;
            if (drawType == "Underlay")
                tiles = underlayTiles;
            else
                tiles = overlayTiles;

            foreach (Tile tile in tiles)
            {
                Image.Position = tile.Position;
                Image.SourceRect = tile.SourceRect;
                Image.Draw(spriteBatch);
            }
        }
    }
}
