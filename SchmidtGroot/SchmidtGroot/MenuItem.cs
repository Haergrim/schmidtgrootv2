﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchmidtGroot
{
    //Classe représetant un item de menu
    //Le type de lien ainsi que l'id permettra la navigation
    public class MenuItem
    {
        public string LinkType;
        public string LinkID;
        public Image Image;
    }
}
