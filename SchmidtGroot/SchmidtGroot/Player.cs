﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace SchmidtGroot
{
    //Classe de gestion du personnage joueur
    public class Player
    {
        public Image Image;
        public Vector2 Velocity;
        public float MoveSpeed;

        public bool IsFighting;

        public Player()
        {
            Velocity = Vector2.Zero;
            IsFighting = false;
        }

        public void LoadContent()
        {
            Image.LoadContent();
        }

        public void UnloadContent()
        {
            Image.UnloadContent();
        }

        public void Update(GameTime gameTime)
        {
            //Si on n'est pas en combat, il sera possible de se déplacer, autrement non
            if(!IsFighting)
            {
                Image.IsActive = true;

                //Interdiction du déplacement diagonal pour faciliter la gestion des collisions
                if(Velocity.X == 0)
                {
                    if (InputManager.Instance.KeyDown(Keys.Down))
                    {
                        Velocity.Y = MoveSpeed * (float)gameTime.ElapsedGameTime.TotalSeconds;
                        Image.SpriteSheetEffect.CurrentFrame.Y = 0;
                    }                
                    else if (InputManager.Instance.KeyDown(Keys.Up))
                    {
                        Velocity.Y = -MoveSpeed * (float)gameTime.ElapsedGameTime.TotalSeconds;
                        Image.SpriteSheetEffect.CurrentFrame.Y = 3;
                    }                
                    else
                        Velocity.Y = 0;
                }

                //On fait deux if afin de pouvoir exécuter les deux blocs séparément
                if(Velocity.Y == 0)
                {
                    if (InputManager.Instance.KeyDown(Keys.Right))
                    {
                        Velocity.X = MoveSpeed * (float)gameTime.ElapsedGameTime.TotalSeconds;
                        Image.SpriteSheetEffect.CurrentFrame.Y = 2;
                    }
                    else if (InputManager.Instance.KeyDown(Keys.Left))
                    {
                        Velocity.X = -MoveSpeed * (float)gameTime.ElapsedGameTime.TotalSeconds;
                        Image.SpriteSheetEffect.CurrentFrame.Y = 1;
                    }
                    else
                        Velocity.X = 0;
                }            

                //Si le personnage ne bouge plus, on désactive l'image (et donc les effets associés)
                if(Velocity.X ==0 && Velocity.Y == 0)
                {
                    Image.IsActive = false;
                }

                Image.Update(gameTime);
                Image.Position += Velocity;
            }
            else
            {
                //On va resetter certaines valeurs spécifiques au combat
                // Créer une classe spécifique au player en combat ? (on lui passerait le player courant par exemple ... à voir)
                Image.IsActive = false;
                Image.SpriteSheetEffect.CurrentFrame.Y = 1;
                Image.SpriteSheetEffect.CurrentFrame.X = 0;
                Image.Update(gameTime);
            }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            Image.Draw(spriteBatch);
        }
    }
}
