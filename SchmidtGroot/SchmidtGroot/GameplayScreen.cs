﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace SchmidtGroot
{
    //Classe d'écran principale du jeu
    public class GameplayScreen : GameScreen
    {
        Player player;
        Map map;

        public override void LoadContent()
        {
            base.LoadContent();

            //On charge la map1 et le joueur par défaut
            XmlManager<Player> playerLoader = new XmlManager<Player>();
            XmlManager<Map> mapLoader = new XmlManager<Map>();
            player = playerLoader.Load("Load/Gameplay/Player.xml");
            map = mapLoader.Load("Load/Gameplay/Maps/Map1.xml");
            player.LoadContent();
            map.LoadContent();
        }

        public override void UnloadContent()
        {
            base.UnloadContent();
            player.UnloadContent();
            map.UnloadContent();
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            //Le joueur pouvant intéragir avec l'environnement, 
            //on update le joueur avant la map afin de pouvoir checker les collisions par exemple
            player.Update(gameTime);
            map.Update(gameTime, ref player);

            if (InputManager.Instance.KeyPressed(Keys.Enter))
                ScreenManager.Instance.ChangeScreens("TitleScreen");


            //Pour test de l'écran de combat
            if (InputManager.Instance.KeyPressed(Keys.F))
                ScreenManager.Instance.ChangeScreens("FightScreen");
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            base.Draw(spriteBatch);
            //On dessine la map en premier afin qu'elle ne soit pas dessinée par dessus le joueur
            map.Draw(spriteBatch, "Underlay");
            player.Draw(spriteBatch);
            //On dessine une seconde fois, les tiles d'overlay, en dernier, afin qu'elles apparaissent par dessus le joueur
            map.Draw(spriteBatch, "Overlay");
        }
    }
}
